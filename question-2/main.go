package main

import "fmt"

func main() {
	recursive(9, nil)
}

// According to this algorithm, it continues to divide the entered number by half according to the remainder recursively.
// First, we check nil because the remainder is not clear, and the algorithm starts from here.
// The algorithm is called again with the remainder of dividing the number by half, and this process continues until the
// remainder of the division by half is equal to the remainder of the currently divided number.
// Meanwhile, the algorithm stops and prints the number we have. In all these processes, we print the remainder to the screen.
func recursive(number int, remainder *int) {
	if remainder != nil {
		remainderByHalf := number / 2
		if remainderByHalf == *remainder {
			fmt.Println(number)

		} else {
			result := number / *remainder
			fmt.Println(result)
			recursive(number, &result)
		}
	} else {
		remainderByHalf := number / 2
		result := number / remainderByHalf
		fmt.Println(result)
		recursive(number, &result)
	}

}

