package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_SortArrayByGivenCharacter_Case1(t *testing.T) {
	// given
	givenArray := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	givenCharacter := "a"
	exceptedResult := []string{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"}

	//when
	SortArrayByGivenCharacter(givenArray, givenCharacter)
	//then

	// The actual result is the same as the givenArray, since the slice operations are progressed with the pointer logic.
	// Well; The function performs the operation on the already existing array and its value changes even if the address of the array remains the same.
	//So we don't need to assertion over a return value.
	//Since this logic will be used in the next tests, it will not be overlooked again.
	assert.Equal(t, exceptedResult, givenArray)
}

func Test_SortArrayByGivenCharacter_Case2(t *testing.T) {
	// given
	givenArray := []string{"asgasg", "gasgas", "qwtwqt", "wqtt", "ef", "gsag", "gdsgsdsdg", "sdgsd", "sdgdsg", "sdggds", "ggg"}
	givenCharacter := "x"
	exceptedResult := []string{"gdsgsdsdg", "asgasg", "gasgas", "qwtwqt", "sdgdsg", "sdggds", "sdgsd", "wqtt", "gsag", "ggg", "ef"}

	//when
	SortArrayByGivenCharacter(givenArray, givenCharacter)
	//then

	assert.Equal(t, exceptedResult, givenArray)
}

func Test_SortArrayByGivenCharacter_Case3(t *testing.T) {
	// given
	givenArray := []string{"aaaaa", "aaaaa", "sgsag", "cxbbsdb", "ef", "cenk", "tetetest", "asgsag", "olsmjg", "sgsag", "opwtogd"}
	givenCharacter := "a"
	exceptedResult := []string{"aaaaa", "aaaaa", "asgsag", "sgsag", "sgsag", "tetetest", "cxbbsdb", "opwtogd", "olsmjg", "cenk", "ef"}

	//when
	SortArrayByGivenCharacter(givenArray, givenCharacter)
	//then

	assert.Equal(t, exceptedResult, givenArray)
}
