package main

import (
	"fmt"
	"sort"
	"strings"
	"unicode/utf8"
)

func main() {

	arr := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}

	SortArrayByGivenCharacter(arr, "a")

	fmt.Println(arr)
}

//This function actually helps us to manage the convenience of Golang's sort Slice, that is, to handle dependencies.
// Array specifies the currently existing array parameter, and character specifies which character we want to sort by.
//It is requested according to a in case-study, but if desired, different characters such as x, y, z, d, c can be selected here.
func SortArrayByGivenCharacter(arr []string, character string) {
	sort.Slice(arr, func(i, j int) bool {
		s1, s2 := arr[i], arr[j]
		count1, count2 := strings.Count(s1, character), strings.Count(s2, character)
		if count1 != count2 {
			return count1 > count2
		}
		return utf8.RuneCountInString(s1) > utf8.RuneCountInString(s2)
	})
}
