# Fill-Labs Case Study

This project is a case study sent by Fill-Labs. The first 3 questions contain algorithm and the last 1 question includes REST API.

## Question-1

In the first question we are asked to sort an array according to the desired character ('a' in this scenario). While sorting the elements with the most a in descending order, if there are equal numbers of a, we need to sort by the length of the word. Here I made 'a' dynamically mutable. Characters can be entered as a parameter to the function and sorting can be done according to each character. This function actually helps us to manage the convenience of Golang's sort Slice, that is, to handle dependencies.Array specifies the currently existing array parameter, and character specifies which character we want to sort by. It is requested according to a in case-study, but if desired, different characters such as x, y, z, d, c can be selected here.

## Question-2

This question has been one of the most challenging questions for me. I wasn't sure what algorithm I should set up. According to this algorithm, it continues to divide the entered number by half according to the remainder recursively. First, we check nil because the remainder is not clear, and the algorithm starts from here. The algorithm is called again with the remainder of dividing the number by half, and this process continues until the remainder of the division by half is equal to the remainder of the currently divided number. Meanwhile, the algorithm stops and prints the number we have. In all these processes, we print the remainder to the screen.

## Question-3
This question allows us to find the most frequently repeated element in an array.
According to this algorithm, I aimed to find the most frequent element in an array. With a map definition, I provide control and identify the most frequent element, navigating the loop one by one, then returning the most frequently used

## Question-4

In this question, I am asked to do a RESTful API and a simple user operation. I wrote the backend with Golang and used PostgreSQL as the database because if I was going to write a user system, I would prefer to use Relational Database (SQL) because in a detailed system, users will have more than one data. In this case it is only mentioned superficially. I used PostgreSQL with Gorm because gorm is an ORM tool and it makes it easier for us to manage and migrate our entities, so we can manage SQL queries over entities.
I used React on the client side. I tried to meet the demands of me with a simple interface.

## Usage
### Backend
In order to use the project, you must first install postgre and pgadmin on docker. To do this, first run the following command in the question-4/backend/ directory;
```
docker-compose up -d
```
Then navigate to the config.yaml file in the backend/config directory. If you haven't changed anything on docker, you can leave it like this. You can run the backend service!
### Client
To run the client side, enter the question-4/client directory and run the following commands respectively.
```
npm install
npm start
```
## Notes from Mert
I was asked to have comment lines in the project, but I did not comply with this practice since we only need to write comments in private functions in clean code practices. It was my primary preference for my practice in question-4 to be clear and concise. I took care to write the code I wrote in a way that can be understood by everyone. Apart from that, I took care to write comments in the first 3 questions. Also
I love coding with Golang. During my internship at Trendyol, I had the opportunity to work on microservice and distributed systems. Here I learned about fun topics such as Kafka, Kubernetes, Docker, CI/CD, Code review, acceptance test and load test. If this project is not enough to evaluate me, I would like to state that my team leader at Trendyol is a reference and I can share his number if you wish.

