import React, { useState, useEffect, useMemo, useRef } from "react";
import UserService from "../services/UserService";
import { useTable } from "react-table";

const UsersList = (props) => {
  const [users, setUsers] = useState([]);
  const userRef = useRef();

  userRef.current = users;

  useEffect(() => {
    retrieveUsers();
  }, []);

 
  const retrieveUsers = () => {
    UserService.getAll()
      .then((response) => {
        setUsers(response.data);
        console.log(response)
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const refreshList = () => {
    retrieveUsers();
  };

  const openUsers = (rowIndex) => {
    const id = userRef.current[rowIndex].ID;

    props.history.push("/users/" + id);
  };

  const newUser = () => {

    props.history.push("/add/");
  };



  const deleteUser = (rowIndex) => {
    const id = userRef.current[rowIndex].ID;

    UserService.remove(id)
      .then((response) => {
        props.history.push("/users");

        let newTutorials = [...userRef.current];
        newTutorials.splice(rowIndex, 1);

        setUsers(newTutorials);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const columns = useMemo(
    () => [
      {
        Header: "ID",
        accessor: "ID",
      },
      {
        Header: "Username",
        accessor: "username",
      },
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Surname",
        accessor: "surname",
      },
      {
        Header: "Mail",
        accessor: "mail",
      },
      {
        Header: "Password",
        accessor: "password",
      },
      {
        Header: "Actions",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.id;
          return (
            <div>

        

              <span onClick={() => openUsers(rowIdx)}>
                <i className="far fa-edit action mr-2"></i>
              </span>

              <span onClick={() => deleteUser(rowIdx)}>
                <i className="fas fa-trash action"></i>
              </span>
            </div>
          );
        },
      },
    ],
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data: users,
  });

  return (
    
    <div className="list row">
    <div class="container">
    <h2>User List</h2>
    <div class="row">
        <div class="col-md-12  text-right mr-2">
            <button type="button" class="btn btn-success" onClick={newUser}>New</button>
            </div>
        </div>
    </div>
    
      <div className="col-md-12 list">
        <table
          className="table table-striped table-bordered"
          {...getTableProps()}
        >
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th {...column.getHeaderProps()}>
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

    
    </div>
  );
};

export default UsersList;
