import React, { useState, useEffect } from "react";
import UserService from "../services/UserService";

const User = props => {
  const initialUserState = {
    id: props.match.params.id,
    username: "",
    name: "",
    surname: "",
    mail: "",
    password: "",
    address: ""
  };
  const [currentUser, setcurrentUser] = useState(initialUserState);
  const [message, setMessage] = useState("");

  const getUser = id => {
    UserService.get(id)
      .then(response => {
        setcurrentUser(response.data);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  useEffect(() => {
    getUser(props.match.params.id);
  }, [props.match.params.id]);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setcurrentUser({ ...currentUser, [name]: value });
  };

  const returnBack = status => {
    props.history.push("/users");
  };

  const updateUser = () => {
    console.log(currentUser)
    UserService.update(props.match.params.id, currentUser)
      .then(response => {
        console.log(response.data);
        setMessage("The user was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const deleteUser = () => {
    UserService.remove(props.match.params.id)
    .then(response => {
      console.log(response.data);
      props.history.push("/users");
    })
    .catch(e => {
      console.log(e);
    });
  };

  return (
    <div>
      {currentUser ? (
        <div className="edit-form">
          <h4>User Detail</h4>
          <form>
            <div className="form-group">
              <label htmlFor="title">Username</label>
              <input
                type="text"
                className="form-control"
                id="username"
                name="username"
                value={currentUser.username}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="title">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={currentUser.name}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="title">Surname</label>
              <input
                type="text"
                className="form-control"
                id="surname"
                name="surname"
                value={currentUser.surname}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="description">Mail</label>
              <input
                type="text"
                className="form-control"
                id="mail"
                name="mail"
                value={currentUser.mail}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="description">Password</label>
              <input
                type="password"
                className="form-control"
                id="password"
                name="password"
                value={currentUser.password}
                onChange={handleInputChange}
              />
            </div>
            <div className="form-group">
              <label htmlFor="description">Address</label>
              <input
                type="text"
                className="form-control"
                id="address"
                name="address"
                value={currentUser.address}
                onChange={handleInputChange}
              />
            </div>

           
          </form>

          

            <button
              className="badge badge-primary mr-2"
              onClick={() => returnBack()}
            >
              Back
            </button>   

          <button className="badge badge-danger mr-2" onClick={deleteUser}>
            Delete
          </button>

          <button
            type="submit"
            className="badge badge-success"
            onClick={updateUser}
          >
            Save
          </button>
          <p>{message}</p>
        </div>
      ) : (
        <div>
          <br />
          <p>Please click on a user...</p>
        </div>
      )}
    </div>
  );
};

export default User;