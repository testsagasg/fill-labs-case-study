import React, { useState } from "react";
import UserService from "../services/UserService";

const AddUser = props => {
  const initialUserState = {
    id: null,
    username: "",
    name: "",
    surname: "",
    mail: "",
    password: "",
    address: ""
  };

  const [user, setUser] = useState(initialUserState);
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const returnBack = status => {
    props.history.push("/users");
  };

  const saveUser = () => {
    var data = {
      username: user.username,
      name: user.name,
      surname: user.surname,
      mail:user.mail,
      password:user.password,
      address: user.address,
    };

    UserService.create(data)
      .then(response => {
        setUser({
          id: response.data.id,
          title: response.data.title,
          description: response.data.description,
          published: response.data.published
        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };

  const newUser = () => {
    setUser(initialUserState);
    setSubmitted(false);
  };

  return (
      

    <div className="submit-form">
      <h2>Add User</h2>
      {submitted ? (
        <div>
          <h4>You submitted successfully!</h4>
          <button onClick={returnBack} className="btn btn-danger mr-2">
            Back
          </button>
          <button className="btn btn-success  mr-2" onClick={newUser}>
            Add
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input
              type="text"
              className="form-control"
              id="username"
              required
              value={user.username}
              onChange={handleInputChange}
              name="username"
            />
          </div>

          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              className="form-control"
              id="name"
              required
              value={user.name}
              onChange={handleInputChange}
              name="name"
            />
          </div>

          <div className="form-group">
            <label htmlFor="surname">Surname</label>
            <input
              type="text"
              className="form-control"
              id="surname"
              required
              value={user.surname}
              onChange={handleInputChange}
              name="surname"
            />
          </div>


          <div className="form-group">
            <label htmlFor="mail">Mail</label>
            <input
              type="text"
              className="form-control"
              id="mail"
              required
              value={user.mail}
              onChange={handleInputChange}
              name="mail"
            />
          </div>

          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              className="form-control"
              id="password"
              required
              value={user.password}
              onChange={handleInputChange}
              name="password"
            />
          </div>

          <div className="form-group">
            <label htmlFor="address">Address</label>
            <input
              type="text"
              className="form-control"
              id="address"
              required
              value={user.address}
              onChange={handleInputChange}
              name="address"
            />
          </div>

          <button onClick={returnBack} className="btn btn-danger mr-2">
            Back
          </button>
          

          <button onClick={saveUser} className="btn btn-success mr-2">
            Create
          </button>
        </div>
      )}
    </div>
  );
};

export default AddUser;
