package model

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Username string `json:"username,omitempty"`
	Name     string `json:"name,omitempty"`
	Surname  string `json:"surname,omitempty"`
	Mail     string `json:"mail,omitempty"`
	Password string `json:"password,omitempty"`
	Address  string `json:"address,omitempty"`
	Status   bool   `json:"status,omitempty"`
}
