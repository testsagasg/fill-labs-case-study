package config

type Config struct {
	Postgre *Postgre
}

type Postgre struct {
	Username string
	Password string
	DbName   string
}
