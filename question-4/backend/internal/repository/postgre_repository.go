package repository

import (
	"fmt"
	"github.com/mertozler/internal/config"
	"github.com/mertozler/internal/model"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
)

type PostgreRepository struct {
	repo *gorm.DB
}

func NewPostgreRepository(config *config.Postgre) *PostgreRepository {
	dbURL := "postgres://" + config.Username + ":" + config.Password + "@localhost:5432/" + config.DbName

	db, err := gorm.Open(postgres.Open(dbURL), &gorm.Config{})

	if err != nil {
		log.Fatalln(err)
	}

	db.AutoMigrate(&model.User{})

	return &PostgreRepository{repo: db}
}

func (p *PostgreRepository) CreateUser(user *model.User) error {
	p.repo.Transaction(func(tx *gorm.DB) error {
		if err := tx.Create(&user).Error; err != nil {
			return err
		}
		return nil
	})
	return nil
}

func (p *PostgreRepository) GetById(id int) model.User {
	var user model.User
	p.repo.First(&user, id)
	return user
}

func (p *PostgreRepository) GetAll() []model.User {
	var users []model.User
	if result := p.repo.Find(&users); result.Error != nil {
		fmt.Println(result.Error)
	}
	return users
}

func (p *PostgreRepository) DeleteById(id int) error {
	var user model.User
	p.repo.Transaction(func(tx *gorm.DB) error {
		if err := tx.Delete(&user, id).Error; err != nil {
			return err
		}
		return nil
	})
	return nil
}

func (p *PostgreRepository) UpdateUser(user *model.User, id int) error {

	p.repo.Transaction(func(tx *gorm.DB) error {
		if err := tx.Model(&user).Where("id = ?", id).Updates(&user).Error; err != nil {
			return err
		}
		return nil
	})
	return nil
}
