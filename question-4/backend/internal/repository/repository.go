package repository

import "github.com/mertozler/internal/model"

type Repository interface {
	CreateUser(user *model.User) error
	GetById(id int) model.User
	GetAll() []model.User
	DeleteById(id int) error
	UpdateUser(user *model.User, id int) error
}
