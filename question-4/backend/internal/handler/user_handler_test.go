package handler

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gofiber/fiber/v2"
	"github.com/mertozler/internal/model"
	"github.com/mertozler/mocks"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_Create_Should_Success_When_Given_Correct_Input(t *testing.T) {
	//given
	requestURL := "http://localhost:8080/api/v1/create"
	repo := mocks.NewRepository(t)
	user := model.User{
		Username: "test",
		Name:     "test name",
		Surname:  "test surname",
		Password: "123",
		Mail:     "testmail",
		Address:  "ev adresi",
		Status:   false,
	}
	repo.On("CreateUser", mock.Anything).Return(nil)
	handler := NewUserHandler(repo)
	app := fiber.New()
	api := app.Group("/api/v1")
	api.Post("/create", handler.Create())

	userRequestJson, _ := json.Marshal(user)
	req := httptest.NewRequest("POST", requestURL, bytes.NewBuffer(userRequestJson))
	req.Header.Set("Content-Type", "application/json")
	//when
	response, _ := app.Test(req, -1)

	//then
	assert.Equal(t, http.StatusOK, response.StatusCode)
}

func Test_Create_Should_Return_Internal_Error_When_Repository_Send_A_Error(t *testing.T) {
	//given
	requestURL := "http://localhost:8080/api/v1/create"
	repo := mocks.NewRepository(t)
	user := model.User{
		Username: "test",
		Name:     "test name",
		Surname:  "test surname",
		Password: "123",
		Mail:     "testmail",
		Address:  "ev adresi",
		Status:   false,
	}
	repo.On("CreateUser", mock.Anything).Return(errors.New("Error while creating user"))
	handler := NewUserHandler(repo)
	app := fiber.New()
	api := app.Group("/api/v1")
	api.Post("/create", handler.Create())

	userRequestJson, _ := json.Marshal(user)
	req := httptest.NewRequest("POST", requestURL, bytes.NewBuffer(userRequestJson))
	req.Header.Set("Content-Type", "application/json")
	//when
	response, _ := app.Test(req, -1)

	//then
	assert.Equal(t, http.StatusInternalServerError, response.StatusCode)
}

func Test_Create_Should_Return_BadRequest_Error_When_Send_Wrong_Data(t *testing.T) {
	//given
	requestURL := "http://localhost:8080/api/v1/create"
	repo := mocks.NewRepository(t)

	handler := NewUserHandler(repo)
	app := fiber.New()
	api := app.Group("/api/v1")
	api.Post("/create", handler.Create())

	req := httptest.NewRequest("POST", requestURL, nil)
	req.Header.Set("Content-Type", "application/json")
	//when
	response, _ := app.Test(req, -1)

	//then
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
}

func Test_GetUser_Should_Success_When_Given_Correct_Input(t *testing.T) {
	//given
	requestURL := "http://localhost:8080/api/v1/get/1"
	repo := mocks.NewRepository(t)
	user := model.User{
		Username: "test",
		Name:     "test name",
		Surname:  "test surname",
		Password: "123",
		Mail:     "testmail",
		Address:  "ev adresi",
		Status:   false,
	}
	repo.On("GetById", mock.Anything).Return(user)
	handler := NewUserHandler(repo)
	app := fiber.New()
	api := app.Group("/api/v1")
	api.Get("/get/:userId", handler.GetUser())

	req := httptest.NewRequest(http.MethodGet, requestURL, nil)
	//when
	response, _ := app.Test(req, -1)

	//then
	assert.Equal(t, http.StatusOK, response.StatusCode)
}

func Test_GetAll_Should_Success(t *testing.T) {
	//given
	requestURL := "http://localhost:8080/api/v1/getall"
	repo := mocks.NewRepository(t)
	userList := []model.User{}
	user := model.User{
		Username: "test",
		Name:     "test name",
		Surname:  "test surname",
		Password: "123",
		Mail:     "testmail",
		Address:  "ev adresi",
		Status:   false,
	}

	userList = append(userList, user)
	repo.On("GetAll", mock.Anything).Return(userList)
	handler := NewUserHandler(repo)
	app := fiber.New()
	api := app.Group("/api/v1")
	api.Get("/getall", handler.GetAll())

	req := httptest.NewRequest(http.MethodGet, requestURL, nil)
	//when
	response, _ := app.Test(req, -1)

	//then
	assert.Equal(t, http.StatusOK, response.StatusCode)
}

func Test_DeleteUser_Should_Success(t *testing.T) {
	//given
	requestURL := "http://localhost:8080/api/v1/delete/1"
	repo := mocks.NewRepository(t)
	repo.On("DeleteById", mock.Anything).Return(nil)
	handler := NewUserHandler(repo)
	app := fiber.New()
	api := app.Group("/api/v1")
	api.Delete("/delete/:userId", handler.DeleteUser())

	req := httptest.NewRequest(http.MethodDelete, requestURL, nil)
	//when
	response, _ := app.Test(req, -1)

	//then
	assert.Equal(t, http.StatusOK, response.StatusCode)
}

func Test_DeleteUser_Should_Return_Error_When_Repository_Send_An_Error(t *testing.T) {
	//given
	requestURL := "http://localhost:8080/api/v1/delete/1"
	repo := mocks.NewRepository(t)
	repo.On("DeleteById", mock.Anything).Return(errors.New("Error while deleting user"))
	handler := NewUserHandler(repo)
	app := fiber.New()
	api := app.Group("/api/v1")
	api.Delete("/delete/:userId", handler.DeleteUser())

	req := httptest.NewRequest(http.MethodDelete, requestURL, nil)
	//when
	response, _ := app.Test(req, -1)

	//then
	assert.Equal(t, http.StatusInternalServerError, response.StatusCode)
}

func Test_UpdateUser_Should_Success_When_Given_Correct_Input(t *testing.T) {
	//given
	requestURL := "http://localhost:8080/api/v1/update/1"
	repo := mocks.NewRepository(t)
	user := model.User{
		Username: "test",
		Name:     "test name",
		Surname:  "test surname",
		Password: "123",
		Mail:     "testmail",
		Address:  "ev adresi",
		Status:   false,
	}
	repo.On("UpdateUser", mock.Anything, 1).Return(nil)
	handler := NewUserHandler(repo)
	app := fiber.New()
	api := app.Group("/api/v1")
	api.Put("/update/:userId", handler.UpdateUser())

	userRequestJson, _ := json.Marshal(user)
	req := httptest.NewRequest("PUT", requestURL, bytes.NewBuffer(userRequestJson))
	req.Header.Set("Content-Type", "application/json")
	//when
	response, _ := app.Test(req, -1)

	//then
	assert.Equal(t, http.StatusOK, response.StatusCode)
}

func Test_UpdateUser_Should_Return_Error_When_Repository_Send_An_Error(t *testing.T) {
	//given
	requestURL := "http://localhost:8080/api/v1/update/1"
	repo := mocks.NewRepository(t)
	user := model.User{
		Username: "test",
		Name:     "test name",
		Surname:  "test surname",
		Password: "123",
		Mail:     "testmail",
		Address:  "ev adresi",
		Status:   false,
	}
	repo.On("UpdateUser", mock.Anything, 1).Return(errors.New("Error while updating user"))
	handler := NewUserHandler(repo)
	app := fiber.New()
	api := app.Group("/api/v1")
	api.Put("/update/:userId", handler.UpdateUser())

	userRequestJson, _ := json.Marshal(user)
	req := httptest.NewRequest("PUT", requestURL, bytes.NewBuffer(userRequestJson))
	req.Header.Set("Content-Type", "application/json")
	//when
	response, _ := app.Test(req, -1)

	//then
	assert.Equal(t, http.StatusInternalServerError, response.StatusCode)
}
