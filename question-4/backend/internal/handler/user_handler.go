package handler

import (
	"github.com/gofiber/fiber/v2"
	"github.com/mertozler/internal/model"
	"github.com/mertozler/internal/repository"
	"github.com/sirupsen/logrus"
	"net/http"
	"strconv"
)

type UserHandler struct {
	repo repository.Repository
}

func NewUserHandler(repo repository.Repository) *UserHandler {
	return &UserHandler{repo: repo}
}

func (u *UserHandler) Create() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		logrus.Info("Create a user handler received a request")
		var user model.User
		if err := c.BodyParser(&user); err != nil {
			logrus.Error("Error while parsing body", err)
			return c.Status(http.StatusBadRequest).JSON(err.Error())
		}

		err := u.repo.CreateUser(&user)
		if err != nil {
			logrus.Error("Error while creating user", err)
			return c.Status(http.StatusInternalServerError).JSON(err.Error())
		}

		return c.Status(fiber.StatusOK).JSON(user)

	}
}

func (u *UserHandler) GetUser() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		userId := c.AllParams()["userId"]
		convertedUserId, _ := strconv.Atoi(userId)
		logrus.Infof("GetUser request received for userId: %v", userId)
		user := u.repo.GetById(convertedUserId)
		return c.Status(http.StatusOK).JSON(user)
	}
}

func (u *UserHandler) GetAll() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		logrus.Infof("Get all users request received")
		users := u.repo.GetAll()
		return c.Status(http.StatusOK).JSON(users)
	}
}

func (u *UserHandler) DeleteUser() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		userId := c.AllParams()["userId"]
		convertedUserId, _ := strconv.Atoi(userId)
		logrus.Infof("Delete user request received for userId: %v", userId)
		err := u.repo.DeleteById(convertedUserId)
		if err != nil {
			logrus.Error("Error while deleting user")
			return c.Status(http.StatusInternalServerError).JSON(err.Error())
		}
		return c.Status(http.StatusOK).Send(nil)
	}
}

func (u *UserHandler) UpdateUser() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		userId := c.AllParams()["userId"]
		convertedUserId, _ := strconv.Atoi(userId)
		logrus.Infof("Update user request received for userId: %v", userId)
		var user model.User
		if err := c.BodyParser(&user); err != nil {
			logrus.Error("Error while parsing body", err)
			return c.Status(http.StatusBadRequest).JSON(err.Error())
		}
		err := u.repo.UpdateUser(&user, convertedUserId)
		if err != nil {
			logrus.Error("Error while updating user")
			return c.Status(http.StatusInternalServerError).JSON(err.Error())
		}
		return c.Status(http.StatusOK).Send(nil)
	}
}
