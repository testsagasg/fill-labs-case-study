package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/mertozler/internal/config"
	"github.com/mertozler/internal/handler"
	"github.com/mertozler/internal/repository"
	"github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"syscall"
)

func init() {
	logrus.SetFormatter(&logrus.JSONFormatter{})
}

func main() {
	configs, err := config.LoadConfig("./configs")
	if err != nil {
		logrus.Fatal("Error while getting configs", err)
	}

	postgreRepo := repository.NewPostgreRepository(configs.Postgre)
	userHandler := handler.NewUserHandler(postgreRepo)

	app := initFiberApp(userHandler)

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		logrus.Infof("Received %s signal", <-c)
		app.Shutdown()
	}()

	err = app.Listen(":8080")
	if err != nil {
		logrus.Fatal("Error while listening port", err)
	}
}

func initFiberApp(userHandler *handler.UserHandler) *fiber.App {
	app := fiber.New()
	//We use cors because we need to grant permission to receive requests from client
	app.Use(cors.New(cors.Config{
		AllowOrigins: "http://localhost:8081",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))

	app.Get("/healtcheck", func(c *fiber.Ctx) error {
		return c.SendString("OK")
	})

	api := app.Group("/api/v1")
	api.Post("/create", userHandler.Create())
	api.Get("/get/:userId", userHandler.GetUser())
	api.Get("/getAll", userHandler.GetAll())
	api.Delete("/delete/:userId", userHandler.DeleteUser())
	api.Put("/update/:userId", userHandler.UpdateUser())
	return app
}
