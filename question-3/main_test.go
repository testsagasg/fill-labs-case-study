package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_MostFrequent_Case_1(t *testing.T) {
	//given
	arr := []string{"apple", "pie", "apple", "red", "red", "red"}

	//when
	freq := mostFrequent(arr)
	//then

	assert.Equal(t, freq, "red")
}

func Test_MostFrequent_Case_2(t *testing.T) {
	//given
	arr := []string{"ahmet", "mehmet", "ahmet", "cenk", "kerem", "merve", "ahmet", "kerem", "merve", "cemal", "kerem", "buse"}

	//when
	freq := mostFrequent(arr)

	//then
	assert.Equal(t, freq, "ahmet")
}

func Test_MostFrequent_Case_3(t *testing.T) {
	//given
	arr := []string{"1", "1", "2", "2", "3", "241", "1", "544", "merve", "5", "3", "3"}

	//when
	freq := mostFrequent(arr)

	//then
	assert.Equal(t, freq, "1")
}
