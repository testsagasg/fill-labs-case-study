package main

import "fmt"

func main() {
	arr := []string{"apple", "pie", "apple", "red", "red", "red"}
	freq := mostFrequent(arr)
	fmt.Println(freq)
}

// According to this algorithm, I aimed to find the most frequent element in an array.
// With a map definition, I provide control and identify the most frequent element,
// navigating the loop one by one, then returning the most frequently used
func mostFrequent(arr []string) string {
	freqChecker := map[string]int{}
	var maxCount int
	var mostFrequentElement string
	for _, element := range arr {
		freqChecker[element]++
		if freqChecker[element] > maxCount {
			maxCount = freqChecker[element]
			mostFrequentElement = element
		}
	}

	return mostFrequentElement
}
